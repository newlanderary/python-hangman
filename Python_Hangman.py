"""
HANGMAN - YEDIDYA NEWLANDER Version
The user gives a word or the computer gives a word at random
The other user needs to figure out what word was selected for the game

WORD: hello (Lower case ONLY)
QUESTION: hello
"""
from time import sleep


def color_print(r, g, b, text):
    print(color(r, g, b, text))


def color(r, g, b, text):
    return "\033[38;2;{};{};{}m{} \033[38;2;255;255m".format(r, g, b, text)


def draw_hangman(points):
    if points == 0:
        color_print(255, 255, 255, "┌-----┐\n|\n|\n|\n|\n|\n┴")
    elif points == 1:
        color_print(255, 180, 200, "┌-----┐\n|     O\n|\n|\n|\n|\n┴")
    elif points == 2:
        color_print(255, 150, 180, "┌-----┐\n|     O\n|    /  \n|\n|\n|\n┴")
    elif points == 3:  # for the easy stage
        color_print(250, 110, 150, "┌-----┐\n|     O\n|    /| \n|\n|\n|\n┴")
    elif points == 4:  # for the easy stage
        color_print(250, 90, 120, "┌-----┐\n|     O\n|    /|\\ \n|\n|\n|\n┴")
    elif points == 5:  # for the easy stage
        color_print(250, 50, 80, "┌-----┐\n|     O\n|    /|\\ \n|     |\n|\n|\n┴")
    elif points == 6:  # for the easy stage
        color_print(250, 20, 60, "┌-----┐\n|     O\n|    /|\\ \n|     |\n|    /\n|\n┴")
    elif points == 7:  # for the easy stage
        color_print(250, 10, 10, "┌-----┐\n|     O\n|    /|\\ \n|     |\n|    / \\ \n|\n┴")
    sleep(1)


def start_game(word, question):
    valid_input = True
    while valid_input:
        word = input("Please enter a word for the game: ").lower()
        sleep(1)
        response = input("Is the word -" + word + "- is the word you want to play? Y/N: ").lower()
        if response == "y":
            valid_input = False
            for i in range(len(word)):
                question = (question + '-')
        else:
            valid_input = True
    sleep(1)
    return word, question


def game(word, question):

    letters = list(word)  # to validate the right guess from the user
    solution = list(question)  # to save the solution created by the user
    play_game, valid_input = True, False
    points_of_player = 0

    while play_game:
        print('\n' * 80)  # to 'clean' the screen
        answer = "".join(solution)
        if answer == word:
            print("CONGRATULATIONS! You WON! The word is: " + word)
            sleep(1)
            break  # exit the loop
        draw_hangman(points_of_player)
        if points_of_player == 7:
            print("We're sorry, but you didn't found the word: " + word)
            break
            sleep(1)
        print("Guess the word: " + answer)
        sleep(1)
        try:
            guess = input("What letter is in the word? ").lower()

            if (ord(guess) < 97 or ord(guess) > 122) or (len(guess) > 1):
                while not valid_input:
                    guess = input("Invalid input. What letter is in the word? ").lower()
                    if (ord(guess) < 97 or ord(guess) > 122) or (len(guess) > 1):
                        valid_input = False
                    else:
                        valid_input = True
                    sleep(1)
            else:
                wrong_guess = False
                for i in range(len(answer)):  # guess = a, word = banana, answer = -a-a-a
                    if guess != letters[i] and guess not in letters:
                        wrong_guess = True
                        break
                    if guess == letters[i]:
                        solution[i] = guess
                        wrong_guess = False
                if wrong_guess:
                    print("Wrong letter. Try again")
                    points_of_player += 1
                sleep(1)
        except TypeError:
            print("There was no letter from the user")


def main():
    word, question = "", ""
    play_game = True
    while play_game:
        word, question = start_game(word, question)
        game(word, question)
        print('\n' * 80)  # to 'clean' the screen
        keep_playing = input("Do you want to keep playing? Y/N: ").lower()
        if keep_playing == "y":
            play_game = True
        else:
            play_game = False
        word, question = "", ""
        sleep(1)
    print("Thanks for playing")
    sleep(1)


if __name__ == "__main__":
    main()
