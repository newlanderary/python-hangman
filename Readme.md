# Hangman - It's fun to play with words

### About the game
The classic game - but through the python console!
The ideal interpreter is onlinegdb.com, the game was developed using that IDE.

This game uses some tricks using the graphical functions of the console.
Highly recommend!

### How it works?
Two players play together - the one enter a word for the game, as if they were using a whiteboard and the first had to choose the game word.
After the word is verified by the first player, the second is invited to join the game after the game was cleared by the program.
There are 3 level of difficulty in the game, with a number of guesses the player has, which draws the hangman on the screen:

**level 1:** 8 guesses

**level 2:** 5 guesses

**level 3:** 3 guesses

If the player figures out the word successfully - he is greeted with a congratulations.
If the player fails the game - the game prompts him with the correct word.

At the end of the game the users has the chance to play again, or to close the program.
It's cute, simple and I hope you'll enjoy it.
